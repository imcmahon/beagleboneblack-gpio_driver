#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h> 
#include <errno.h>
#include "beaglebone_gpio.h"

// test GPIO1_28 (P9 pin 12) as input

typedef struct {
	volatile void *gpio_addr;
	volatile unsigned int *oe_reg;
	volatile unsigned int *setdataout_reg;
	volatile unsigned int *clrdataout_reg;
} bb_gpio_port;

typedef struct {
	bb_gpio_port *port;
	unsigned char pin_num;
	unsigned short control_offset;
} bb_gpio_pin;


bb_gpio_port *ports[4];
volatile void *control_module = NULL;

off_t start_addr_for_port(int port) {
	switch(port) {
		case 0:
			return GPIO0_START_ADDR;
			break;
		case 1:
			return GPIO1_START_ADDR;
			break;
		case 2:
			return GPIO2_START_ADDR;
			break;
		case 3:
			return GPIO3_START_ADDR;
			break;
		default:
			return -1;
			break;
	}
}

void setup() {
	int i;
	int fd = open("/dev/mem", O_RDWR);


	for(i=0; i<4; i++) {
		ports[i] = malloc(sizeof(bb_gpio_port));

		ports[i]->gpio_addr = mmap(0, GPIO_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, start_addr_for_port(i));

		if(ports[i]->gpio_addr == MAP_FAILED) {
			perror("Unable to map GPIO");
			exit(1);
		}

		ports[i]->oe_reg = ports[i]->gpio_addr + GPIO_OE;
		ports[i]->setdataout_reg = ports[i]->gpio_addr + GPIO_SETDATAOUT;
		ports[i]->clrdataout_reg = ports[i]->gpio_addr + GPIO_CLEARDATAOUT;
	}

	control_module = mmap(0, CONTROL_MODULE_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, CONTROL_MODULE_START_ADDR);

	if(control_module == MAP_FAILED) {
		perror("Unable to map Control Module");
		exit(1);
	}

	close(fd);
}

void teardown() {
	int i;

	for(i=0; i<4; i++) {
		free(ports[i]);
	}
}

/*
	configures pin direction and pullup enable and direction
	
	oe_reg - address of the (mmap'd) GPIO_OE register for the port the pin we're looking to set up is in.

	valid values for mode are:
		O	-	output
		I	-	input, no pullu/d
		U	-	input, pullup enabled
		D	-	input, pulldown enabled
*/
void configure_pin(bb_gpio_pin *pin, char mode) {
	volatile unsigned int *control_reg = control_module + pin->control_offset; 
	switch(mode) {
		case 'O':
			*(pin->port->oe_reg) &= ~(1 << pin->pin_num); // 0 in OE is output enable
			*control_reg = PIN_MODE7 | PIN_PULLUD_DISABLED | PIN_RX_DISABLED;
			break;
		case 'I':
			*(pin->port->oe_reg) |= (1 << pin->pin_num); // 1 in OE is input
			*control_reg = PIN_MODE7 | PIN_PULLUD_DISABLED | PIN_RX_ENABLED;
			break;
		case 'U':
			*(pin->port->oe_reg) |= (1 << pin->pin_num); // 1 in OE is input
			*control_reg = PIN_MODE7 | PIN_PULLUD_ENABLED | PIN_PULLUP | PIN_RX_ENABLED;
			break;
		case 'D':
			*(pin->port->oe_reg) |= (1 << pin->pin_num); // 1 in OE is input
			*control_reg = PIN_MODE7 | PIN_PULLUD_ENABLED | PIN_PULLDOWN | PIN_RX_ENABLED;
			break;
		default:
			break;
	}
}


int main(int argc, char *argv[]) {
	unsigned int reg;

	setup();

	bb_gpio_pin *pin = malloc(sizeof(bb_gpio_pin));

	// setup GPIO1_28
	pin->port 				= ports[1];
	pin->pin_num			= 28;
	pin->control_offset 	= 0x878;

	configure_pin(pin, 'O');

	printf("Start toggling GPIO1_28\n");
	while(1) {
		*(pin->port->setdataout_reg) = (1 << pin->pin_num);
		usleep(10);
		*(pin->port->clrdataout_reg) = (1 << pin->pin_num);
		usleep(10);
	}

	free(pin);
	teardown();

	return 0;
}
